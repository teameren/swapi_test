//
//  AppDelegate.h
//  swapi_test
//
//  Created by eren on 7.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

