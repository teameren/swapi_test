//
//  People.h
//  swapi_test
//
//  Created by eren on 8.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface People : NSObject

@property (nonatomic, strong) NSMutableArray<Person *> *people;

-(void)parseFromDictionary:(NSDictionary *)dictData;
-(void)save;
+(People *)readData;
-(void)deleteData;
@end

NS_ASSUME_NONNULL_END
