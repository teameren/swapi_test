//
//  People.h
//  swapi_test
//
//  Created by eren on 8.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *height;
@property (nonatomic, strong) NSString *mass;
@property (nonatomic, strong) NSString *hair_color;
@property (nonatomic, strong) NSString *skin_color;
@property (nonatomic, strong) NSString *eye_color;
@property (nonatomic, strong) NSString *birth_year;
@property (nonatomic, strong) NSString *gender;

// I wont use mapper 
-(void)parseFromDictionary:(NSDictionary*)data;
@end

NS_ASSUME_NONNULL_END
