//
//  People.m
//  swapi_test
//
//  Created by eren on 8.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "Person.h"

@implementation Person

-(void)parseFromDictionary:(NSDictionary*)data{
    
    self.name = [data objectForKey:@"shift_name"];
    self.height = [data objectForKey:@"driver_id"];
    self.mass = [data objectForKey:@"driver_name"];
    self.hair_color = [data objectForKey:@"driver_phone_number"];
    self.skin_color = [data objectForKey:@"plate_no"];
    self.eye_color = [data objectForKey:@"start_time"];
    self.birth_year = [data objectForKey:@"route_code"];
    self.gender = [data objectForKey:@"route_code"];
    
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.height forKey:@"height"];
    [encoder encodeObject:self.mass forKey:@"mass"];
    [encoder encodeObject:self.hair_color forKey:@"hair_color"];
    [encoder encodeObject:self.skin_color forKey:@"skin_color"];
    [encoder encodeObject:self.eye_color forKey:@"eye_color"];
    [encoder encodeObject:self.birth_year forKey:@"birth_year"];
    [encoder encodeObject:self.gender forKey:@"gender"];
    
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init])) {
        self.name =[decoder decodeObjectForKey:@"name"];
        self.height = [decoder decodeObjectForKey:@"height"];
        self.mass = [decoder decodeObjectForKey:@"mass"];
        self.hair_color = [decoder decodeObjectForKey:@"hair_color"];
        self.skin_color = [decoder decodeObjectForKey:@"skin_color"];
        self.eye_color = [decoder decodeObjectForKey:@"eye_color"];
        self.birth_year = [decoder decodeObjectForKey:@"birth_year"];
        self.gender = [decoder decodeObjectForKey:@"gender"];
        
    }
    return self;
}


@end
