//
//  BaseViewController.h
//  swapi_test
//
//  Created by eren on 7.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

-(void)showIndicator;
-(void)dismissIndicator;

@end

NS_ASSUME_NONNULL_END
