//
//  BaseViewController.m
//  swapi_test
//
//  Created by eren on 7.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController{
    UIView *viewIndicatorBackground;
    UIActivityIndicatorView *activityIndicator ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    viewIndicatorBackground = [[UIView alloc] init];
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
}

-(void)showIndicator {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    CGRect indiFrame = [UIScreen mainScreen].bounds;
    if (self.view.frame.size.width > self.view.frame.size.height) {
        indiFrame.size.height = indiFrame.size.width;
        indiFrame.size.width = self.view.frame.size.height;
    }
    [viewIndicatorBackground setFrame:indiFrame];
    [viewIndicatorBackground setBackgroundColor:[UIColor blackColor]];
    [viewIndicatorBackground setAlpha:0.6];
    viewIndicatorBackground.layer.zPosition = NSIntegerMax;
    
    activityIndicator.alpha = 1;
    [viewIndicatorBackground addSubview:activityIndicator];
    activityIndicator.center = CGPointMake(indiFrame.size.width/2, indiFrame.size.height/2);
    if ([UIApplication sharedApplication].keyWindow) {
        [[UIApplication sharedApplication].keyWindow addSubview:viewIndicatorBackground];
    }else{
        [[[[UIApplication sharedApplication] delegate] window] addSubview:viewIndicatorBackground];
    }
    
    [activityIndicator startAnimating];
}

-(void)dismissIndicator {
    [activityIndicator stopAnimating];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [viewIndicatorBackground removeFromSuperview];
}


@end
