//
//  main.m
//  swapi_test
//
//  Created by eren on 7.01.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
